import entities.Category;
import entities.Product;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    private static final int ADD_PRODUCT = 1;
    private static final int SHOW_PRODUCTS = 2;
    private static final int EDIT_PRODUCT = 3;
    private static final int REMOVE_PRODUCT = 4;
    private static final int EXIT = 5;

    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory =
                Persistence.createEntityManagerFactory("HibernateConnection");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Scanner sc = new Scanner(System.in);
        boolean error;
        int userChoice = -1;

        while (userChoice != EXIT) {
            error = true;
            while (error) {
                showUsersOptions();
                try {
                    userChoice = sc.nextInt();
                    sc.nextLine();
                    if (userChoice >= 1 && userChoice <= 5) {
                        error = false;
                        switch (userChoice) {
                            case ADD_PRODUCT:
                                addTheProduct(entityManager, sc);
                                break;
                            case SHOW_PRODUCTS:
                                showProducts(entityManager);
                                break;
                            case EDIT_PRODUCT:
                                modifyTheProduct(entityManager, sc);
                                break;
                            case REMOVE_PRODUCT:
                                removeProduct(entityManager, sc);
                                break;
                        }
                    } else {
                        System.err.println("There is not such an option. Try again");
                    }
                } catch (InputMismatchException e) {
                    System.err.println("Incorrect value. Try again");
                    sc.nextLine();
                }
            }
        }
        System.out.println("bye bye");

    }

    private static void showUsersOptions() {
        System.out.println("1.\tAdd product");
        System.out.println("2.\tShow products");
        System.out.println("3.\tEdit product");
        System.out.println("4.\tRemove product");
        System.out.println("5.\tClose application");
    }

    private static void showProducts(EntityManager entityManager) {
        System.out.println("Id |\t\t" + String.format("%20s", "Name |")
                + "\t\t" + String.format("%20s", "Category |")
                + "\t\t" + String.format("%8s", "Price |")
                + "\t\t" + String.format("%8s", "Rate | ") + "Description");
        entityManager.createQuery("FROM Product ", Product.class)
                .getResultStream()
                .map(e -> String.format("%4s", e.getId() + " |")
                        + "\t\t" + String.format("%20s", e.getName() + " |")
                        + "\t\t" + String.format("%20s", e.getCategory().getName() + " |")
                        + "\t\t" + String.format("%8s", e.getPrice() + " |")
                        + "\t\t" + String.format("%8s", e.getRate() + " | ") + e.getDescription())
                .forEach(System.out::println);
    }

    private static void removeProduct(EntityManager entityManager, Scanner sc) {
        System.out.println("Select id of the product you want to remove:");
        showProducts(entityManager);
        int idOfTheProduct = sc.nextInt();
        sc.nextLine();
        entityManager.getTransaction().begin();
        entityManager.remove(entityManager.find(Product.class, idOfTheProduct));
        entityManager.getTransaction().commit();
        System.out.println("Product deleted successfully!");
    }

    private static void modifyTheProduct(EntityManager entityManager, Scanner sc) {
        System.out.println("Select the product, which You want to modify:");
        showProducts(entityManager);
        int idOfTheProduct = sc.nextInt();
        sc.nextLine();
        Product product = entityManager.find(Product.class, idOfTheProduct);
        System.out.println("Fill in new data. If you dont want to change the data, click the enter:");
        Query query = entityManager.createQuery("UPDATE Product p SET p.name = ?1, p.category = :cat, p.price = ?2, p.rate = ?3, p.description = ?4 WHERE p.id = ?5");
        System.out.println("Set the name of the product (" + product.getName() + "):");
        String name = sc.nextLine();
        if (!name.isEmpty()) {
            query.setParameter(1, name);
        } else {
            query.setParameter(1, product.getName());
        }
        System.out.println("Select category from the list(" + product.getCategory() + "):");
        entityManager.createQuery("FROM Category", Category.class).getResultList().forEach(System.out::println);
        String cat = sc.nextLine();
        if (!cat.isEmpty()) {
            query.setParameter("cat", entityManager.find(Category.class, Integer.valueOf(cat)));
        } else {
            query.setParameter("cat", product.getCategory());
        }
        System.out.println("Set the price of the product(" + product.getPrice() + "):");
        String price = sc.nextLine();
        if (!price.isEmpty()) {
            query.setParameter(2, BigDecimal.valueOf(Double.valueOf(price)));
        } else {
            query.setParameter(2, product.getPrice());
        }
        System.out.println("Set the rate of the product from 1 to 10(" + product.getRate() + "):");
        String rate = sc.nextLine();
        if (!rate.isEmpty()) {
            while (Integer.valueOf(rate) < 1 || Integer.valueOf(rate) > 10) {
                System.out.println("Incorrect value, please enter the rate form 1 to 10");
                rate = sc.nextLine();
            }
            query.setParameter(3, Integer.valueOf(rate));
        } else {
            query.setParameter(3, product.getRate());
        }
        System.out.println("Set the description:");
        String desc = sc.nextLine();
        if (!desc.isEmpty()) {
            query.setParameter(4, desc);
        } else {
            query.setParameter(4, product.getDescription());
        }
        query.setParameter(5, idOfTheProduct);
        entityManager.getTransaction().begin();
        query.executeUpdate();
        entityManager.getTransaction().commit();
        System.out.println("Record updated successfully!");

    }

    private static void addTheProduct(EntityManager entityManager, Scanner sc) {
        Product product = createNewProduct(entityManager, sc);
        entityManager.getTransaction().begin();
        entityManager.persist(product);
        entityManager.getTransaction().commit();
        System.out.println("Product added successfully!");
    }

    private static Product createNewProduct(EntityManager entityManager, Scanner sc) {
        Product product = new Product();
        System.out.println("Set the name of the product:");
        product.setName(sc.nextLine());
        System.out.println("Select category from the list:");
        entityManager.createQuery("FROM Category", Category.class).getResultList().forEach(System.out::println);
        product.setCategory(entityManager.find(Category.class, sc.nextInt()));
        System.out.println("Set the price of the product:");
        product.setPrice(BigDecimal.valueOf(sc.nextDouble()));
        System.out.println("Set the rate of the product from 1 to 10");
        int rate = sc.nextInt();
        sc.nextLine();
        while (rate < 1 || rate > 10) {
            System.out.println("Incorrect value, please enter the rate form 1 to 10");
            rate = sc.nextInt();
            sc.nextLine();
        }
        product.setRate(rate);
        System.out.println("Do you want to include description of the product? (y/n)");
        String answer = sc.nextLine();
        if (answer.matches("[yY]|YES|yes")) {
            System.out.println("Set the description:");
            product.setDescription(sc.nextLine());
        }
        return product;
    }
}
