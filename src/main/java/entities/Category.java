package entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@ToString(exclude = "products")
@Entity
@Table(name = "categories")
public class Category {
    @Id
    @Column(name = "cat_id")
    private int id;
    @Column(name = "cat_name")
    private String name;
    @OneToMany(mappedBy = "category")
    private List<Product> products;

}
