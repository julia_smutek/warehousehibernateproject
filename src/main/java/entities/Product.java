package entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@ToString
@Entity
@Table(name = "products")
public class Product {
    @Id
    @Column(name = "prod_id")
    private int id;
    @Column(name = "prod_name")
    private String name;
    @ManyToOne
    @JoinColumn(name = "prod_category_id")
    private Category category;
    @Column(name = "prod_price")
    private BigDecimal price;
    @Column(name = "prod_rate")
    private int rate;
    @Column(name = "prod_desc")
    private String description;
}
