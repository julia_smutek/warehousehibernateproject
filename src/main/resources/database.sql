CREATE DATABASE warehouse;

CREATE TABLE categories(
  cat_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  cat_name VARCHAR(250)
);

CREATE TABLE products (
  prod_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  prod_name VARCHAR(250) NOT NULL,
  prod_category_id INT NOT NULL,
  prod_price DECIMAL(5,2) NOT NULL ,
  prod_rate INT NOT NULL,
  FOREIGN KEY (prod_category_id) REFERENCES categories(cat_id)
);

ALTER TABLE products ADD COLUMN prod_desc VARCHAR(500);


